# JavaFXVSCodeConfig

Configuration d'un projet JavaFX dans VSCocde ou VSCodium

Note : vous aurez besoin d'installer l'extension ```Debugger for Java```

1. Créez un répertoire ```.vscode``` à la racine de votre projet s'il n'existe pas déjà
1. Créez un fichier ```launch.json``` dans ce répertoire en [suivant l'exemple](.vscode/launch.json)
1. Créez un fichier ```settings.json``` dans ce répertoire en [suivant l'exemple](.vscode/settings.json)

Editer les chemins dans les fichiers ```.vscode/launch.json``` et ```.vscode/settings.json```. La configuration des chemins doit permettre un fonctionnement en salles TP.

Paramètres de ```launch.json```
- ```type``` : la valeur doit être ```java```
- ```name``` : vous pouvez choisir le nom que vous voulez
- ```request``` : laissez ```launch```
- ```mainClass```: adaptez pour faire pointer vers votre classe principale. Si vous indiquez ```${file}```, la classe correspondante au fichier courant sera utilisée
- ```vmArgs``` : adaptez le chemin suivant votre installation de JavaFX

## En cas de problème

Il peut être parfois nécessaire de faire un nettoyage en cas de problème. Pour cela lancez la palette de commandes (ctrl + shift + P) puis exécutez la commande ```Java: Clean Java Language Server Workspace```

## macOS

Sur macOS, ajoutez l'option ```-Dprism.lcdtext=off``` à ```vmArgs``` si vous rencontrez des artefacts visuels sur l'interface.